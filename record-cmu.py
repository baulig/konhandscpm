#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig
# Based on: https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md


###BUILDIN###
import os
from io import BytesIO

##INSTALLED##
import numpy as np
from PIL import Image
import cv2
import tensorflow as tf

####LOCAL####
import utils.tools.retag as rt
from utils.tools import load_json
from utils.tools.hmap import create_heatmaps
from utils.tfio import *

#TODO create recorder class see record-konhands

DELIMITER = '/'

def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog
	)
	parser.add_argument('-a', '--annotations', help="JSON-files of CMU dataset.", type=str, nargs='+', metavar='JSON')
	parser.add_argument('-o', '--output', help="Filepath to record output.", type=str, default='data/cmu.record', metavar='FILE')
	parser.add_argument('-B', '--boxsize', help='The box size of input images the model has to expect.', type=int, default=512, metavar='INT')
	parser.add_argument('-O', '--offset', help='Offset on cropping.', type=float, default=0.2, metavar='INT')
	return parser


def crop_image(imgpath, joints, offset=0.2):
	image = cv2.imread(imgpath)
	imshape = image.shape[:2]
	mins = joints[:,1::-1].min(axis=0)
	maxs = joints[:,1::-1].max(axis=0)
	bbshape = maxs - mins
	offsets = bbshape * offset
	mins = np.clip(mins - offsets, (0,0), imshape).astype(np.uint32)
	maxs = np.clip(maxs + offsets, (0,0), imshape).astype(np.uint32)
	crop = image[mins[0]:maxs[0], mins[1]:maxs[1], :]
	joints[:,0] = (joints[:,0] - mins[1]) / crop.shape[1]
	joints[:,1] = (joints[:,1] - mins[0]) / crop.shape[0]
	return crop, joints


def record_image(image, imname, features, boxsize=512, base_key='image'):
	shape = image.shape
	if shape[0] > shape[1]:
		scale = boxsize / shape[0]
		image = cv2.resize(image, (0, 0), fx=scale, fy=scale)
		uv_scale = (image.shape[1]/boxsize, 1.0)
	else:
		scale = boxsize / image.shape[1]
		image = cv2.resize(image, (0, 0), fx=scale, fy=scale)
		uv_scale = (1.0, image.shape[0]/boxsize)

	canvas = np.zeros((boxsize, boxsize, 3))
	canvas[:image.shape[0], :image.shape[1], :] = image
	
	image = Image.fromarray(canvas[:,:,::-1].astype(np.uint8), 'RGB')
	encoded = BytesIO()
	image.save(encoded, 'JPEG')
	features[DELIMITER.join([base_key, 'filename'])] = tf_string(imname.encode('utf8'))
	features[DELIMITER.join([base_key, 'shape'])] = tf_ints(canvas.shape)
	features[DELIMITER.join([base_key, 'encoded'])] = tf_string(encoded.getvalue())
	return features, uv_scale


def record_joints(joints, scale, features, base_key='joint'):
	joints[:,0] = joints[:,0] * scale[0]
	joints[:,1] = 1.0 - joints[:,1] * scale[1]
	features[DELIMITER.join([base_key, 'u'])] = tf_floats(joints[:,0])
	features[DELIMITER.join([base_key, 'v'])] = tf_floats(joints[:,1])
	features[DELIMITER.join([base_key, 'd'])] = tf_floats(joints[:,2])
	return features, joints


def record_heatmaps(uvd, features, boxsize=512, base_key='joint'):
	heatmaps = create_heatmaps(boxsize//8, uvd)
	features[DELIMITER.join([base_key, 'heatmaps'])] = tf_floats(heatmaps.reshape(-1))
	return features, heatmaps


def record_tf_example(imgpath, joints, features, boxsize=512, offset=0.2):
	crop, joints = crop_image(imgpath, joints, offset)
	imname = os.path.basename(imgpath)
	_, scale = record_image(crop, imname, features, boxsize)
	_, joints = record_joints(joints, scale, features)
	record_heatmaps(joints, features, boxsize)
	return tf.train.Example(features=tf.train.Features(feature=features))


def create_tf_record(output, anno_paths, boxsize=512, offset=0.2):
	goods = 0
	errs = 0
	key = 0
	with tf.python_io.TFRecordWriter(output) as writer:
		for anno_path in anno_paths:
			anno = load_json(anno_path)
			joints = np.array(anno['hand_pts'])
			impath = anno_path.replace('.json', '.jpg')
			try:
				tf_example = record_tf_example(impath, joints, {}, boxsize, offset)
				writer.write(tf_example.SerializeToString())
				goods += 1
				print("INFO: Record example {}".format(anno_path))
			except:
				errs += 1
				if key != 'a':
					key = input("WARNING: An error occured. Skip example {}. \n\tProceed anyway? (y) [y|n|a]:".format(anno_path))
				if key == 'n':
					raise	
	return goods, errs


def main(args):
	goods, errs = create_tf_record(args.output, args.annotations, args.boxsize, args.offset)
	print("{} created with {} files. {} rejected".format(args.output, goods, errs))

if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)
