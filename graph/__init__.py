# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig

"""
"""


##INSTALLED##
import numpy as np
import tensorflow as tf

def load_weights(filename, sess, saver=None):
	"""
	Load model weights from file to tf.Session.
	Supports checkpoints and frozen_graphes (*.pb).

	Args:
		filename:	(str) String to model file.
		sess:		(tf.Session) Tensorflow Session as target.
		saver:		(tf.train.Saver) Tensorflow Saver, in case of loading from checkpoint.
		            If None: A temporary saver will be created.
	"""
	if filename.endswith('.pb'):
		with tf.gfile.GFile(filename, "rb") as f:
			graph_def = tf.GraphDef()
			graph_def.ParseFromString(f.read())
		with tf.Graph().as_default():
			tf.import_graph_def(graph_def, name="prefix")
	else:
		if saver:
			saver.restore(sess, filename)
		else:
			tf.train.Saver().restore(sess, filename)
	return


def check_weights(sess):
	"""
	Prints all trainable variables of loaded model.

	Args:
		sess:	(tf.Session) Tensorflow Session
	"""
	for variable in tf.trainable_variables():
		with tf.variable_scope('', reuse=True):
			var = tf.get_variable(variable.name.split(':0')[0])
			print(variable.name, np.mean(sess.run(var)))
	return

