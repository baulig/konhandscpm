# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""
Provides classes and functions 
to build and manage a Tensorflow adaption of
Convolutional Pose Machine.
http://github.com/shihenw/convolutional-pose-machines-release

Original: Tim Ho (http://github.com/timctho/convolutional-pose-machines-tensorflow)
Modified: Gerald Baulig
"""

##INSTALLED##
import tensorflow as tf
####LOCAL####
from . import load_weights


class CPM(object):
	"""
	Provides functions to build a Tensorflow-Graph of Convolutional Pose Machine.
	Warning: This class makes use of Python's prototyping!
	Warning: This class doesn't support multiple instances, yet!
	Note: Make sure that all required build functions are called before you make use of this object.
	"""

	def __init__(self, input_img=None, boxsize=512, batch_size=1, img_type='RGB'):
		"""
		Constructor of CPM. 
		Creates input layer for images.

		Args:
			input_img:  (tf.Tensor or tf.Placeholder) for input images. 
			            If None: tf.Placholder will be created.
			boxsize:    (uint) The boxsize of the input image (width = hight).
			batch_size: (uint) The batch size. How many images process in parallel.
			img_type:   (str) Type of images. Supports: (RGB, BGR or GRAY).

		Raises:
			ValueError:	When unsupported img_type is given.
		"""
		self.boxsize = boxsize
		self.batch_size = batch_size
		self.img_type = img_type

		if isinstance(input_img, tf.Tensor):
			self.input_img = input_img
		elif self.img_type in ['RGB', 'BGR']:
			self.input_img = tf.placeholder(dtype=tf.float32,
											shape=[self.batch_size, self.boxsize, self.boxsize, 3],
											name='input_img')
		elif self.img_type == 'GRAY':
			self.input_img = tf.placeholder(dtype=tf.float32,
											shape=[self.batch_size, self.boxsize, self.boxsize, 1],
											name='input_img')
		else:
			raise ValueError('ERROR: Unspported image type: {}'.format(img_type))
		pass


	def build_graph(self, stages=6, layers=21, trainable=False):
		"""
		Build staged CPM graph.

		Args:
			stages:    (uint) Count of stages. (min 2)
			layers:    (uint) Count of layers. Determine how many layers the heatmap has.
			trainable: (bool) Determine whether a trainable graph should be initialized or not.

		Returns:
			(tf.Tensor) The final heatmap of output layer.		
		"""
		self.stage_heatmap = []
		self.stages = stages
		self.layers = layers
		self.training = trainable

		with tf.variable_scope('sub_stages'):
			sub_conv1 = tf.layers.conv2d(inputs=self.input_img,
										filters=64,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv1')
			sub_conv2 = tf.layers.conv2d(inputs=sub_conv1,
										filters=64,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv2')
			sub_pool1 = tf.layers.max_pooling2d(inputs=sub_conv2,
										pool_size=[2, 2],
										strides=2,
										padding='valid',
										name='sub_pool1')
			sub_conv3 = tf.layers.conv2d(inputs=sub_pool1,
										filters=128,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv3')
			sub_conv4 = tf.layers.conv2d(inputs=sub_conv3,
										filters=128,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv4')
			sub_pool2 = tf.layers.max_pooling2d(inputs=sub_conv4,
										pool_size=[2, 2],
										strides=2,
										padding='valid',
										name='sub_pool2')
			sub_conv5 = tf.layers.conv2d(inputs=sub_pool2,
										filters=256,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv5')
			sub_conv6 = tf.layers.conv2d(inputs=sub_conv5,
										filters=256,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv6')
			sub_conv7 = tf.layers.conv2d(inputs=sub_conv6,
										filters=256,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv7')
			sub_conv8 = tf.layers.conv2d(inputs=sub_conv7,
										filters=256,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv8')
			sub_pool3 = tf.layers.max_pooling2d(inputs=sub_conv8,
										pool_size=[2, 2],
										strides=2,
										padding='valid',
										name='sub_pool3')
			sub_conv9 = tf.layers.conv2d(inputs=sub_pool3,
										filters=512,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv9')
			sub_conv10 = tf.layers.conv2d(inputs=sub_conv9,
										filters=512,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv10')
			sub_conv11 = tf.layers.conv2d(inputs=sub_conv10,
										filters=512,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv11')
			sub_conv12 = tf.layers.conv2d(inputs=sub_conv11,
										filters=512,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv12')
			sub_conv13 = tf.layers.conv2d(inputs=sub_conv12,
										filters=512,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv13')
			sub_conv14 = tf.layers.conv2d(inputs=sub_conv13,
										filters=512,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_conv14')
			self.sub_stage_img_feature = tf.layers.conv2d(inputs=sub_conv14,
										filters=128,
										kernel_size=[3, 3],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='sub_stage_img_feature')

		with tf.variable_scope('stage_1'):
			conv1 = tf.layers.conv2d(inputs=self.sub_stage_img_feature,
										filters=512,
										kernel_size=[1, 1],
										strides=[1, 1],
										padding='valid',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='conv1')
			self.stage_heatmap.append(tf.layers.conv2d(inputs=conv1,
										filters=self.layers,
										kernel_size=[1, 1],
										strides=[1, 1],
										padding='valid',
										activation=None,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='stage_heatmap'))

		for stage in range(2, self.stages + 1):
			self.__middle_conv(stage)
		self.final_heatmap = self.stage_heatmap[-1]
		return self.final_heatmap


	def __middle_conv(self, stage):
		with tf.variable_scope('stage_' + str(stage)):
			self.current_featuremap = tf.concat([self.stage_heatmap[stage - 2],
										self.sub_stage_img_feature],
										axis=3)
			mid_conv1 = tf.layers.conv2d(inputs=self.current_featuremap,
										filters=128,
										kernel_size=[7, 7],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='mid_conv1')
			mid_conv2 = tf.layers.conv2d(inputs=mid_conv1,
										filters=128,
										kernel_size=[7, 7],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='mid_conv2')
			mid_conv3 = tf.layers.conv2d(inputs=mid_conv2,
										filters=128,
										kernel_size=[7, 7],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='mid_conv3')
			mid_conv4 = tf.layers.conv2d(inputs=mid_conv3,
										filters=128,
										kernel_size=[7, 7],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='mid_conv4')
			mid_conv5 = tf.layers.conv2d(inputs=mid_conv4,
										filters=128,
										kernel_size=[7, 7],
										strides=[1, 1],
										padding='same',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='mid_conv5')
			mid_conv6 = tf.layers.conv2d(inputs=mid_conv5,
										filters=128,
										kernel_size=[1, 1],
										strides=[1, 1],
										padding='valid',
										activation=tf.nn.relu,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='mid_conv6')
			self.stage_heatmap.append(tf.layers.conv2d(inputs=mid_conv6,
										filters=self.layers,
										kernel_size=[1, 1],
										strides=[1, 1],
										padding='valid',
										activation=None,
										kernel_initializer=tf.contrib.layers.xavier_initializer(),
										trainable=self.training,
										name='mid_conv7'))
		pass


	def build_hmap_loss(self, hmap_gt=None, depth_gt=None, d_stages=0, hmap_dscale=8):
		"""
		Build heatmap loss function.
		Create input layer for ground truth heatmap.

		Args:
			hmap_gt:     (tf.Tensor or tf.Placeholder)
			             For input of ground truth heatmaps.
			             If None: tf.Placeholder will be created.
			depth_gt:    (tf.Tensor or tf.Placeholder)
			             Contains depth-values multiplied layer-wise on gt_hmap.
			             On last Stage only!
			             (optional)
			d_stages:    (uint) Count of D-Stages.
			             Determines how many Stages are used for depth prediction.
			             (optional)
			hmap_dscale: (uint) Shrink factor of heatmap. (Must be 8)
			             hmap_size = boxsize / hmap_dscale

		Returns:
			(tf.Tensor) The total heatmap loss of all stages.
		"""
		self.hmap_size = self.boxsize//hmap_dscale
		self.stage_hmap_loss = [0 for _ in range(self.stages)]
		self.total_hmap_loss = 0
		self.d_stages = d_stages

		if isinstance(hmap_gt, tf.Tensor):
			self.hmap_gt = hmap_gt
		else:
			self.hmap_gt = tf.placeholder(dtype=tf.float32,
				shape=[self.batch_size, self.hmap_size, self.hmap_size, self.layers],
				name='hmap_gt')

		if d_stages <= 0:
			self.depth_gt = tf.constant(1.0, dtype=tf.float32)
		elif isinstance(depth_gt, tf.Tensor):
			self.depth_gt = depth_gt
		else:
			self.depth_gt = tf.placeholder(dtype=tf.float32,
				shape=[self.batch_size, 0, 0, self.layers],
				name='depth_gt')

		with tf.variable_scope('hmap_loss'):
			for stage in range(self.stages - d_stages):
				varn = 'stage{}_hmap_loss'.format(stage + 1)
				with tf.variable_scope(varn):
					self.stage_hmap_loss[stage] = tf.nn.l2_loss(
						tf.divide(self.stage_heatmap[stage] - self.hmap_gt, self.batch_size), 
						name='l2_loss')
				tf.summary.scalar(varn, self.stage_hmap_loss[stage])
			self.pos_heatmap = self.stage_hmap_loss[stage]
				
			for stage in range(self.stages - d_stages, self.stages):
				varn = 'd_stage{}_hmap_loss'.format(stage + 1)
				with tf.variable_scope(varn):
					self.stage_hmap_loss[stage] = tf.nn.l2_loss(
						tf.divide(self.stage_heatmap[stage] - tf.multiply(self.hmap_gt, self.depth_gt), self.batch_size), 
						name='l2_loss')
				tf.summary.scalar(varn, self.stage_hmap_loss[stage])
			self.depth_heatmap = self.stage_hmap_loss[stage]

			with tf.variable_scope('total'):
				for stl in self.stage_hmap_loss:
					self.total_hmap_loss += stl

		tf.summary.scalar('total_hmap_loss', self.total_hmap_loss)
		self.final_hmap_loss = self.stage_hmap_loss[-1]
		return self.total_hmap_loss


	def build_eucl_loss(self, uvd_gt=None, joint_dict=None, selected=None, dim=3):
		"""
		Build euclidean loss function.
		Create input layer for ground truth uvd (U, V, Depth).

		Args:
			uvd_gt:     (tf.Tensor or tf.Placeholder)
			            For input of ground truth uvd.
			            If None: tf.Placeholder will be created.
			joint_dict: (dict) A dictonary of Labels (see utils.tools.Label)
			selected:   (list) A list of label names.
			            Determine which labels are used for evaluation.
			dim:        (uint) Determine how many dimensions are used for evaluation.
			            2 = evaluate uv
			            3 = evaluate uvd
		Returns:
			(tf.Tensor) The total euclidean loss of all stages.
		"""
		self.total_eucl_loss = 0
		self.stage_eucl_loss = [0 for _ in range(self.stages)]
		self.stage_uvd = []

		if isinstance(uvd_gt, tf.Tensor):
			self.uvd_gt = uvd_gt
		elif joint_list:
			self.uvd_gt = tf.placeholder(dtype=tf.float32,
				shape=[self.batch_size, len(joint_list), 3],
				name='uvd_gt')
		else:
			self.uvd_gt = tf.placeholder(dtype=tf.float32,
				shape=[self.batch_size, self.layers, 3],
				name='uvd_gt')

		with tf.variable_scope('stage_uvd'):
			for stage in range(self.stages):
				batch_uvd = []
				for batch in range(self.batch_size):
					c = tf.argmax(tf.reduce_max(self.stage_heatmap[stage][batch], axis=0), output_type=tf.int32)
					r = tf.argmax(tf.reduce_max(self.stage_heatmap[stage][batch], axis=1), output_type=tf.int32)
					d =	[self.stage_heatmap[stage][batch, r[i], c[i], i] for i in range(self.layers)]
					u = tf.cast(c / self.stage_heatmap[stage].shape[2], tf.float32)
					v = tf.cast((-r + self.stage_heatmap[stage].shape[1]) / self.stage_heatmap[stage].shape[1], tf.float32)
					batch_uvd.append(tf.stack([u,v,d], axis=1))
				self.stage_uvd.append(tf.stack(batch_uvd, axis=0))
		
		with tf.variable_scope('eucl_loss'):
			for stage in range(self.stages):
				varn = 'stage{}'.format(stage + 1)
				with tf.variable_scope(varn):
					if selected:
						for batch in range(self.batch_size):
							for i, key in enumerate(selected):
								idx = joint_dict[key].idx
								self.stage_eucl_loss[stage] += tf.norm(self.stage_uvd[stage][batch,idx,:dim] - self.uvd_gt[batch,i,:dim])
					else:
						self.stage_eucl_loss[stage] = tf.norm(self.stage_uvd[stage][:,:,:dim] - self.uvd_gt[:,:,:dim])
				tf.summary.scalar(varn, self.stage_eucl_loss[stage])

			with tf.variable_scope('total'):
				for stl in self.stage_eucl_loss:
					self.total_eucl_loss += stl

		tf.summary.scalar('total', self.total_eucl_loss)
		self.final_uvd = self.stage_uvd[-1]
		self.final_eucl_loss = self.stage_eucl_loss[-1]
		return self.total_eucl_loss


	def build_train_op(self, lr, lr_decay_rate, lr_decay_step, loss, optimizer='RMSProp'):
		"""
		Build training optimizer.

		Args:
			lr:            (float) Initial learning rate.
			lr_decay_rate: (float) Decay factor of learning rate.
			lr_decay_step: (uint) Count of steps to next decay.
			loss:          (tf.Tensor) A loss function.
			               See: build_hmap_loss() or build_eucl_loss()

		Returns:
			(tf.Tensor) The training optimizer loss. 
		"""
		with tf.variable_scope('train'):
			self.global_step = tf.train.get_or_create_global_step()
			self.cur_lr = tf.train.exponential_decay(
				lr,
				global_step=self.global_step,
				decay_rate=lr_decay_rate,
				decay_steps=lr_decay_step)
			tf.summary.scalar('global_learning_rate', self.cur_lr)

			self.train_op = tf.contrib.layers.optimize_loss(
				loss=loss,
				global_step=self.global_step,
				learning_rate=self.cur_lr,
				optimizer=optimizer)
		return self.train_op


	def set_global_step(self, sess, global_step):
		"""
		Overwrite the global step counter.
		This effects the current learning rate!

		Note:
			Make sure that build_train_op() was called before!
		
		Args:
			sess:        (tf.Session) A Session of tensorflow.
			global_step: (uint) The new step counter.
		"""
		with tf.variable_scope('train', reuse=True):
			sess.run(tf.assign(tf.get_variable('global_step', dtype=tf.int64), global_step))


	def load_weights(self, filepath, sess, saver=None, finetune=False):
		"""
		Loads model weights to session.

		Note:
			Make sure that all required build functions were called before!

		Args:
			filepath: (str) String to Pickle file or checkpoint.
			saver:    (tf.train.Saver) Tensorflow Saver for loading checkpoints.
			          If None: A temporary saver will be created.
			finetune: (bool) Create finetune nodes or not.
			          Only relevant on pickle file.
		"""
		if filepath.endswith('.pkl'):
			self.__load_pickle(filepath, sess, finetune)
		else:
			load_weights(filepath, sess, saver)
		pass


	def __load_pickle(self, filepath, sess, finetune=False):
		import pickle
		with open(filepath, 'rb') as fid:
			weights = pickle.load(fid, encoding='latin1')

		with tf.variable_scope('', reuse=True):
			## Pre stage conv
			# conv1-5
			grp_pat = 'sub_stages/sub_conv{}/{}'
			pkl_pat = 'conv{}_{}'
			conv_layers = [3, 3, 5, 5, 3]
			offset = 0
			for idx, layers in enumerate(conv_layers):
				idx += 1
				for layer in range(1, layers):
					conv_kernel = tf.get_variable(grp_pat.format(layer + offset, 'kernel'))
					conv_bias = tf.get_variable(grp_pat.format(layer + offset, 'bias'))

					loaded_kernel = weights[pkl_pat.format(idx, layer)]
					loaded_bias = weights[pkl_pat.format(idx, layer) + '_b']

					sess.run(tf.assign(conv_kernel, loaded_kernel))
					sess.run(tf.assign(conv_bias, loaded_bias))
				offset += layers - 1

			# conv5_3_CPM
			conv_kernel = tf.get_variable('sub_stages/sub_stage_img_feature/kernel')
			conv_bias = tf.get_variable('sub_stages/sub_stage_img_feature/bias')

			loaded_kernel = weights['conv5_3_CPM']
			loaded_bias = weights['conv5_3_CPM_b']

			sess.run(tf.assign(conv_kernel, loaded_kernel))
			sess.run(tf.assign(conv_bias, loaded_bias))

			## stage 1
			conv_kernel = tf.get_variable('stage_1/conv1/kernel')
			conv_bias = tf.get_variable('stage_1/conv1/bias')

			loaded_kernel = weights['conv6_1_CPM']
			loaded_bias = weights['conv6_1_CPM_b']

			sess.run(tf.assign(conv_kernel, loaded_kernel))
			sess.run(tf.assign(conv_bias, loaded_bias))

			if not finetune:
				conv_kernel = tf.get_variable('stage_1/stage_heatmap/kernel')
				conv_bias = tf.get_variable('stage_1/stage_heatmap/bias')

				loaded_kernel = weights['conv6_2_CPM']
				loaded_bias = weights['conv6_2_CPM_b']

				sess.run(tf.assign(conv_kernel, loaded_kernel))
				sess.run(tf.assign(conv_bias, loaded_bias))

				## Stage 2 and behind
				grp_pat = 'stage_{}/mid_conv{}/{}'
				pkl_pat = 'Mconv{}_stage{}{}'
				for stage in range(2, self.stages + 1):
					for layer in range(1, 8):
						conv_kernel = tf.get_variable(grp_pat.format(stage, layer, 'kernel'))
						conv_bias = tf.get_variable(grp_pat.format(stage, layer, 'bias'))

						loaded_kernel = weights[pkl_pat.format(layer, stage, '')]
						loaded_bias = weights[pkl_pat.format(layer, stage, '_b')]

						sess.run(tf.assign(conv_kernel, loaded_kernel))
						sess.run(tf.assign(conv_bias, loaded_bias))
		pass
