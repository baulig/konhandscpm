#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig


###BUILDIN###
import os
##INSTALLED##
import numpy as np
import cv2
import tensorflow as tf
####LOCAL####
import utils.tools.vizcv as viz
import utils.tools.hmap as hmap
from utils.tfio import load_examples
from utils.tfops import decode_image, split_str
from utils.tools import delta, load_labels
from graph.cpm import CPM
from graph import load_weights, check_weights

##TODO create Training class!!!


def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog
	)
	parser.add_argument('-r', '--records', help="Tensorflow record files of dataset.", type=str, nargs='*', default=['data/train.record'], metavar='FILE')
	parser.add_argument('-f', '--features', help="JSON of feature description.", type=str, nargs='*', default=['config/head_feature.json', 'config/right_feature.json'], metavar='JSON')
	parser.add_argument('-j', '--joints', help="JSON of joint labels.", type=str, default='config/joints.json', metavar='JSON')
	parser.add_argument('-m', '--model', help="Model to finetune.", type=str, default='model/cpm_hand.pkl', metavar='FILE')
	parser.add_argument('-e', '--epochs', help="How many epochs of training to run (>=1).", type=int, default=1, metavar='INT')
	parser.add_argument('-t', '--traindir', help="Output dir where model files shall be saved to.", type=str, default='train/', metavar='DIR')
	parser.add_argument('-l', '--logdir', help="Output dir where log files shall be saved to.", type=str, default=None, metavar='DIR')
	parser.add_argument('-s', '--steps', help="How many steps of training to run (-1 = ignore steps).", type=int, default=-1, metavar='INT')
	parser.add_argument('-c', '--checkpoint', help="How many steps before next checkpoint of saving (0 = never).", type=int, default=1000, metavar='INT')
	parser.add_argument('-N', '--name', help="Model name - used as suffix.", type=str, default='cpm_hand', metavar='STR')
	parser.add_argument('-B', '--boxsize', help="The box size of input images the model has to expect.", type=int, default=512, metavar='INT')
	parser.add_argument('-T', '--imgtype', help="The type of input images the model has to expect.", type=str, choices=['BGR', 'RGB', 'GRAY'], default='BGR')
	parser.add_argument('-S', '--stages', help="Stages in CPM model", type=int, default=6, metavar='INT')
	parser.add_argument('-L', '--learnrate', help="Learning rates. (init, decay, step)", type=float, nargs=3, default=[0.0001, 0.5, 10000], metavar='FLOAT')
	parser.add_argument('-V', '--visualize', help="Visualize each step.", type=str, nargs='*', choices=['gt_batch', 'gt_depth', 'est_batch', 'est_depth', 'diff_depth'], default=[], metavar='VIEW')
	parser.add_argument('-G', '--global_step', help="Overwrite global step counter (-1 = use original)", type=int, default=-1, metavar='INT')
	parser.add_argument('-D', '--depth_stages', help="How many depth-stages to train?", type=int, default=0, metavar='INT')	
	parser.add_argument('--seed', help="Seed for random generator.", type=int, default=None, metavar='INT')
	return parser

	
def print_stats(gl_step, cur_lr, st_loss, to_loss, t_elap):
	stats = 'Step: {} ----- Cur_lr: {:1.7f} ----- Time: {:>2.2f} sec.'.format(gl_step, cur_lr, t_elap)
	losses = ' | '.join(['loss: {:>7.2f}'.format(loss) for loss in st_loss])
	losses += ' | Total loss: {}'.format(to_loss)
	print(stats)
	print(losses)


def show(gt_img, gt_hmap, est_hmap, joint_dict=None, wins=['gt_batch', 'gt_depth', 'est_batch', 'est_depth']):
	def destroy():
		for w in show.wins.values():
			cv2.destroyWindow(w)
		pass
	show.destroy = destroy
	show.wins = {}

	for i in range(gt_img.shape[0]):
		img = gt_img[i].copy() #cv2.cvtColor(gt_img, cv2.COLOR_BGR2RGB)
		img = (img + 0.5) * 127

		if 'gt_batch' in wins:
			gt = hmap.sum_hmaps(gt_hmap[i], joint_dict)
			gt = cv2.resize(gt, img.shape[:2])
			if joint_dict:
				uvd = hmap.find_maxima(gt_hmap[i], as_uvd=True)
				gt = viz.draw_joints(gt, uvd, joint_dict, min_d=0.25)
			gt = gt * 127
			win = 'batch {} ground truth'.format(i)
			show.wins['gt_batch{}'.format(i)] = win
			cv2.imshow(win, np.clip(img + gt, 0, 255).astype(np.uint8))

		if 'gt_depth' in wins:
			win = 'depth {} ground truth'.format(i)
			show.wins['gt_depth{}'.format(i)] = win
			cv2.imshow(win, np.clip(gt_hmap[i,:,:,-2]*255, 0, 255).astype(np.uint8))

		if 'est_batch' in wins:
			pred = hmap.sum_hmaps(est_hmap[i], joint_dict)
			pred = cv2.resize(pred, img.shape[:2])
			if joint_dict:
				uvd = hmap.find_maxima(est_hmap[i], as_uvd=True)
				hm = viz.draw_joints(pred, uvd, joint_dict, min_d=0.25)
			pred = pred * 127
			win = 'batch {} estimation'.format(i)
			show.wins['est_batch{}'.format(i)] = win
			cv2.imshow(win, np.clip(img + pred, 0, 255).astype(np.uint8))

		if 'est_depth' in wins:
			win = 'depth {} estimation'.format(i)
			show.wins['est_depth{}'.format(i)] = win
			cv2.imshow(win, np.clip(est_hmap[i,:,:,-2]*255, 0 , 255).astype(np.uint8))

		if 'diff_depth' in wins:
			win = 'depth {} loss'.format(i)
			show.wins['diff_depth{}'.format(i)] = win
			dmaploss = (gt_hmap[i,:,:,-2] - est_hmap[i,:,:,-2]) * 127 + 127
			cv2.imshow(win, np.clip(dmaploss, 0 , 255).astype(np.uint8))
	return cv2.waitKey(1)


def training(sess, cpm, **kwargs):
	## known extract kwargs
	steps = kwargs.get('steps', -1)
	checkpoint = kwargs.get('checkpoint', 0)
	joint_dict = kwargs.get('joint_dict', None)
	visualize = kwargs.get('visualize', False)
	## Start threads?
	coord = tf.train.Coordinator()
	threads = tf.train.start_queue_runners(coord=coord)
	summary = tf.summary.merge_all()

	gl_step = 0
	try:
		## Loop up to step if set, otherwise run dataset N epochs
		while steps < 0 or gl_step < steps:
			model_stat = [
				cpm.input_img,
				cpm.hmap_gt,
				cpm.final_heatmap,
				cpm.depth_gt,
				cpm.stage_hmap_loss,	
				cpm.total_hmap_loss, 
				cpm.cur_lr,
				cpm.global_step,
				summary,
				cpm.train_op
			]
			(image, hmap_gt, hmap_est, _, st_loss, to_loss, cur_lr, gl_step, summ, _) = sess.run(model_stat)

			## Show training info & write logs ##
			print_stats(gl_step, cur_lr, st_loss, to_loss, delta())
			if cpm.logger:
				cpm.logger.add_summary(summ, gl_step)
			if visualize:
				key = show(image, hmap_gt, hmap_est, joint_dict, visualize)
				if key is 27:
					visualize = []
					show.destroy()
				elif key is ord('q'):
					exit("Quit request!")

			# Save checkpoint
			if not checkpoint:
				pass
			elif gl_step % checkpoint:
				pass
			else:
				cpm.saver.save(sess=sess, save_path=cpm.save_path, global_step=gl_step)
				print("\nCheckpoint...\n")

	except tf.errors.OutOfRangeError:
		print("INFO: Dataset expired at step {}!".format(gl_step))
	else:
		print("Training done.")
	finally:
		## Join threads?
		coord.request_stop()
		coord.join(threads)

	print("\nSave training stage...")
	cpm.saver.save(sess=sess, save_path=cpm.save_path, global_step=gl_step)
	print("Training stage saved.\n")
	pass


def prep_examples(features, records, imgtype='BGR', boxsize=512, epochs=1, use_depth=False, seed=None):
	with tf.variable_scope('examples'):
		examples = load_examples(features, records, epochs=epochs)
		for example in examples:
			fn = example.image_filename.tensor
			ext = split_str(fn, '.', -1) #'png'
			shape = example.image_shape.tensor
			image = example.image_encoded.tensor
			image, _, _ = decode_image(image, shape, ext, boxsize)
			## Invert Channels
			if imgtype == 'BGR':
				image = image[:,:,::-1]
			image = tf.cast(image, tf.float32)
			## Post adjustments
			if seed:
				image = tf.image.random_hue(image, 0.05, seed=seed)
				image = tf.image.random_contrast(image, 0.95, 1.05, seed=seed)
				image = tf.image.random_saturation(image, 0.95, 1.05, seed=seed)
				image = tf.image.random_brightness(image, 0.05, seed=seed)
			## Clip
			image = tf.add(tf.divide(image, 255), -0.5)
			image = tf.clip_by_value(image, -0.5, 0.5)
			## Set graphed image
			example.image = image
			## Heatmaps
			example.hmap = example.joint_heatmaps.tensor
			## Depth
			if use_depth:
				example.hmap = tf.concat([example.hmap, example.depthmap.tensor], axis=-1)
				example.depth = example.joint_d.tensor / -100.0 + 1
				example.depth = tf.expand_dims(tf.expand_dims(example.depth, axis=0), axis=0)
				example.depth = tf.concat([example.depth, tf.constant([[[1.0]]], dtype=tf.float32)], axis=-1)
			pass
		## Prep Returns
		input_img = tf.stack([e.image for e in examples], axis=0, name='input_img')
		hmap_gt = tf.stack([e.hmap for e in examples], axis=0, name='hmap_gt')
		batch_size = len(examples)
		if use_depth:
			depth_gt = tf.stack([e.depth for e in examples], axis=0, name='depth_gt')
		else:
			depth_gt = None
	return input_img, hmap_gt, depth_gt, batch_size

def main(args):
	global VISUALIZE
	delta()
	## Check args ##
	if not os.path.isdir(args.traindir):
		raise ValueError("ERROR: traindir {} is not a directory!".format(args.traindir))
	for d in args.records:
		if not os.path.isfile(d):
			raise ValueError("ERROR: Record file '{}' doesn't exist!".format(d))

	## Lookup for checkpoint ##
	ckpt = tf.train.latest_checkpoint(args.traindir)
	if ckpt:
		print("\n---Checkpoint detected!---")
		print("Actually selected is {}".format(args.model))
		cont = input("Wanna continue with checkpoint {} instead? [y/n/q]:".format(ckpt)).lower()
		if cont.startswith('y'):
			args.model = ckpt
		elif cont.startswith('q'):
			exit("\nSo you prefare to quit...")
		print("\nProceed with {}...\n".format(args.model))
			
	## Load lables ##
	print("\nLoad labels from json...")
	joint_dict = load_labels(args.joints)
	print("Labels loaded.\n")

	## Load Features and Record to Examples ##
	print("\nPrepare examples...")
	input_img, hmap_gt, depth_gt, batch_size = prep_examples(
		args.features,
		args.records,
		args.imgtype,
		args.boxsize,
		args.epochs,
		args.depth_stages,
		args.seed)
	print("Examples loaded.\n")

	## Build graph ##
	print("\nBuild graph...")
	hmap_layers = len(joint_dict)+1 if args.depth_stages else len(joint_dict)
	cpm = CPM(
		input_img=input_img,
		boxsize=args.boxsize,
		batch_size=batch_size,
		img_type=args.imgtype)
	cpm.build_graph(args.stages, hmap_layers, trainable=True)
	loss = cpm.build_hmap_loss(hmap_gt, depth_gt, args.depth_stages)
	cpm.build_train_op(args.learnrate[0], args.learnrate[1], args.learnrate[2], loss, optimizer='RMSProp')
	print("Graph ready.\n")

	## Session ##
	print("\nInit session...")
	init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
	with tf.Session(config=tf.ConfigProto(device_count={'GPU':1}, allow_soft_placement=True)) as sess:
		## Init all vars
		sess.run(init_op)		
		## Create tensorboard
		cpm.save_path = os.path.join(args.traindir, args.name)
		cpm.saver = tf.train.Saver(max_to_keep=2, filename=args.name)
		if args.logdir:
			cpm.logger = tf.summary.FileWriter(args.logdir, sess.graph)
		else:
			cpm.logger = None

		## Restore pretrained weights ##
		print("\nLoad model...")
		if not args.model:
			pass
		elif args.model.endswith('.pkl'):
			cpm.load_weights(args.model, sess)
		else:
			load_weights(args.model, sess, cpm.saver)
		check_weights(sess)
		print("Model loaded.\n")

		if args.global_step >= 0:
			cpm.set_global_step(sess, args.global_step)

		# Process
		training(sess, cpm, 
			steps=args.steps, 
			checkpoint=args.checkpoint,
			joint_dict=joint_dict,
			visualize=args.visualize)

if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)

