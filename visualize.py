#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig

###BUILDIN###
import os
###INSTALLED###
import numpy as np
import cv2
import tensorflow as tf
###LOCAL###
from utils.tfio import load_examples
from utils.tfops import decode_image, split_str
from utils.tools import load_labels
from utils.tools.vizcv import *


##ASCII##
ESC = 27
ENTER = 13
SPACE = 32


def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog
	)
	parser.add_argument('-r', '--records', nargs='+', help='Tensorflow record files of dataset.', type=str, metavar='RECORD')
	parser.add_argument('-f', '--features', nargs='*', help='JSON of feature description.', type=str, default=['config/head_feature.json', 'config/right_feature.json'], metavar='JSON')
	parser.add_argument('-l', '--labels', help='JSON of label description.', type=str, default='config/joints.json', metavar='JSON')
	parser.add_argument('-o', '--outdir', help='Output dir where images might be saved to.', type=str, default='output/', metavar='DIR')
	parser.add_argument('-x', '--headless', help='Run headless mode - requires output dir!', nargs='?', type=bool, const=True, metavar='TRUE')
	parser.add_argument('-s', '--seed', help='Seed for random adjustments.', type=int, default=None, metavar='INT')
	return parser


def render_session(sess, tf_img, tf_imgname, tf_uvd, tf_hmaps, tf_dmap, labels):
	if tf_dmap is not None:
		tensors = [tf_img, tf_imgname, tf_uvd, tf_hmaps, tf_dmap]
		canvas, imgname, uvd, hmaps, dmap = sess.run(tensors)
		dmap *= 255
		dmap = cv2.cvtColor(dmap, cv2.COLOR_GRAY2BGR)
		dmap = cv2.resize(dmap, canvas.shape[:2])
		canvas = np.clip(canvas - dmap, 0, 255)
	else:
		tensors = [tf_img, tf_imgname, tf_uvd, tf_hmaps]
		canvas, imgname, uvd, hmaps = sess.run(tensors)
	canvas = draw_hmaps(canvas, hmaps, labels, img_ratio=1)
	canvas = draw_joints(canvas, uvd, labels, colscl=255)
	return canvas, imgname


#MAIN---------------------------------------------#
def main(args):
	## Load Labels
	labels = load_labels(args.labels)
	
	## Load Features and Record to Examples
	print('\nPrepare examples...')
	examples = load_examples(args.features, args.records, epochs=1)
	for example in examples:
		## Prep image
		fn = example.image_filename.tensor
		shape = example.image_shape.tensor
		ext = split_str(fn, '.', -1) #'png'
		image = example.image_encoded.tensor
		image, _, _ = decode_image(image, shape, ext)
		## Randomize
		if args.seed:
			image = tf.image.random_hue(image, 0.125, seed=args.seed)
			image = tf.image.random_contrast(image, 0.8, 1.2, seed=args.seed)
			image = tf.image.random_saturation(image, 0.8, 1.2, seed=args.seed)
			image = tf.image.random_brightness(image, 0.125, seed=args.seed)
		example.image = image[:,:,::-1]
		## Joints
		u = example.joint_u.tensor
		v = example.joint_v.tensor
		d = example.joint_d.tensor
		example.uvd = tf.stack([u, v, d], axis=1)
		
	print('Examples loaded.\n')

	## Init global vars?
	init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())

	## Init Session
	with tf.Session() as sess:	
		sess.run(init_op)

		## Start threads?
		coord = tf.train.Coordinator()
		threads = tf.train.start_queue_runners(coord=coord)

		try:
			key = -1
			while key is not ESC:
				for example in examples:
					## Switch context
					uvd = example.uvd
					hmap = example.joint_heatmaps.tensor
					fn = example.image_filename.tensor
					img = example.image
					if 'depthmaps' in example.keys():
						dmap = example.depthmap.tensor
					else:
						dmap = None

					canvas, imgname = render_session(sess, img, fn, uvd, hmap, dmap, labels)
					imgname = imgname.decode('utf8')
					print(imgname)
					key = show('visualize', canvas, [ESC, ENTER, ord('s')])
					if key is ESC:
						break
					elif key is ord('s'):
						path = os.path.join(args.outdir, imgname)
						save(path, canvas)
						print('Saved: {}'.format(path))
					else:
						pass


		except tf.errors.OutOfRangeError:
			print('INFO: Dataset end!')
		finally:
			## Join threads?
			coord.request_stop()
			coord.join(threads)
	pass
	

if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)

	


