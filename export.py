#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig
#
# The original freeze_graph function:
# from tensorflow.python.tools.freeze_graph import freeze_graph 

###BUILDIN###
import os
##INSTALLED##
import tensorflow as tf

#TODO figure out correct output nodes!!!

def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog
	)
	parser.add_argument('-m', '--model_dir', help="Model folder to export", type=str, default='train/', metavar='DIR')
	parser.add_argument('-o', '--output', help="Filepath for export file", type=str, default='export/frozen_model.pb', metavar='FILEPATH')
	parser.add_argument('-n', '--node_names', help="The name of the output nodes, comma separated.", type=str, nargs='*', default=[], metavar='NODES')
	return parser


def freeze_graph(model_dir, node_names):
	"""Extract the sub graph defined by the output nodes and convert 
	all its variables into constant 
	Args:
		model_dir: the root folder containing the checkpoint state file
		node_names: a string, containing all the output node's names, 
							comma separated
	"""
	if not tf.gfile.Exists(model_dir):
		raise ValueError(
			"Export directory doesn't exists. Please specify an export "
			"directory: %s" % model_dir)

	if not node_names:
		raise ValueError("You need to supply the name of a node to --node_names.")

	# We retrieve our checkpoint fullpath
	model_dir = os.path.abspath(model_dir)
	input_checkpoint = os.path.join(model_dir, tf.train.get_checkpoint_state(model_dir).model_checkpoint_path)

	# We start a session using a temporary fresh Graph
	with tf.Session(graph=tf.Graph()) as sess:
		# We import the meta graph in the current default Graph
		saver = tf.train.import_meta_graph(input_checkpoint + '.meta', clear_devices=True)
		# We restore the weights
		saver.restore(sess, input_checkpoint)

		# We use a built-in TF helper to export variables to constants
		graph_def = tf.graph_util.convert_variables_to_constants(
			sess, # The session is used to retrieve the weights
			tf.get_default_graph().as_graph_def(), # The graph_def is used to retrieve the nodes 
			node_names # The output node names are used to select the usefull nodes
		)
	return graph_def

if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	graph_def = freeze_graph(args.model_dir, args.node_names)
	output = os.path.abspath(args.output)

	# Finally we serialize and dump the output graph to the filesystem
	with tf.gfile.GFile(output, 'wb') as f:
		f.write(graph_def.SerializeToString())
	print("%d ops in the final graph." % len(graph_def.node))

