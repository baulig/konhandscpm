#!/usr/bin/env python3

from utils.tools.indx import argparser, main

if __name__ == '__main__':
	args, _ = argparser(prog=__file__).parse_known_args()
	main(args)
