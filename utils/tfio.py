# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig

"""
Provids classes and function for tensorflow input/output.
For writting and reading Tensorflow-Recorder files.
"""


##INSTALLED##
import tensorflow as tf
####LOCAL####
from .tools import load_json


DTYPE = {
	'float16':tf.float16,
	'float32':tf.float32,
	'float64':tf.float64,
	'bfloat16':tf.bfloat16,
	'complex64':tf.complex64,
	'complex128':tf.complex128,
	'int8':tf.int8,
	'uint8':tf.uint8,
	'uint16':tf.uint16,
	'uint32':tf.uint32,
	'uint64':tf.uint64,
	'int16':tf.int16,
	'int32':tf.int32,
	'int64':tf.int64,
	'bool':tf.bool,
	'string':tf.string,
	'qint8':tf.qint8,
	'quint8':tf.quint8,
	'qint16':tf.qint16,
	'quint16':tf.quint16,
	'quint16':tf.qint32,
	'resource':tf.resource,
	'variant':tf.variant
}


def tf_string(value):
	"""
	Converts string to tf.train.Feature.
	Args:
		value: (byte string) to convert.
	Returns:
		tf.train.Feature
	"""
	return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def tf_strings(value):
	"""
	Converts list of strings to tf.train.Feature.
	Args:
		value: (list of byte string) to convert.
	Returns:
		tf.train.Feature
	"""
	return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))


def tf_int(value):
	"""
	Converts int to tf.train.Feature.
	Args:
		value: (int) to convert.
	Returns:
		tf.train.Feature
	"""
	return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def tf_ints(value):
	"""
	Converts list of int to tf.train.Feature.
	Args:
		value: (list of int) to convert.
	Returns:
		tf.train.Feature
	"""
	return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def tf_float(value):
	"""
	Converts float to tf.train.Feature.
	Args:
		value: (byte string) to convert.
	Returns:
		tf.train.Feature
	"""
	return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def tf_floats(value):
	"""
	Converts list of floats to tf.train.Feature.
	Args:
		value: (list of floats) to convert.
	Returns:
		tf.train.Feature
	"""
	return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def read(recorde_files, parser_dict, reader=None, epochs=1):
	"""
	Reads a Tensorflow-Record-File.
	Args:
		record_files: (list of strings) as pathes to record files.
		parser_dict:  (dict) A dictonary with parser instructions.
		              e.g. config/head_feature.json
		reader:       (tf.TFRecordReader) Optional reader instance.
		epochs:       (uint) How many epochs the records to be read.
	Returns:
		(tf.Tensor) as iterator of examples.
	"""
	if not reader:
		reader = tf.TFRecordReader()
	recorde_files = tf.train.string_input_producer(recorde_files, num_epochs=epochs)
	_, example = reader.read(recorde_files)
	return tf.parse_single_example(example, features=parser_dict)


class Feature():
	"""
	Helper class to keep parser infos of Feature-Tensors.
	"""
	def __init__(self, name, in_type, cast_type=None, fix_len=None, tensor=None):
		"""
		Construct a feature parse instructor.
		Args:
			name:      (string) Name of the feature.
			in_type:   (string) Input type of the feature.
			cast_type: (string) Optional cast type.
			fix_len:   (shape) Optional shape for fix array length.
			           Otherwise array length is variable, but undefined.
			tensor:    (tf.Tensor) to parse.
		"""
		self.name = name
		self.in_type = DTYPE[in_type]
		self.cast_type = DTYPE.get(cast_type, None)
		self.fix_len = fix_len
		self.tensor = tensor
		pass


class Example():
	"""
	Wrapper class to parse and iterate Tensorflow-Records. 
	"""
	def __init__(self, feature_json=None, reader=None, epochs=1):
		"""
		Construct instance of Example.
		Args:
			feature_json: (string) to JSON-file of feature description. (optional)
			reader:       (tf.TFRecordReader) Instance of reader. (optional)
			epochs:       (uint) How many epochs the record is to read.
		Note:
			If feature_json is given record will be loaded.
		See:
			Example.load(...)
		"""
		self.__feature_dict = {}
		self.__parser_dict = {}
		self.__epochs = max(1, epochs)

		if reader:
			self.reader = reader
		else:
			self.reader = tf.TFRecordReader()

		if feature_json:
			self.load(feature_json)
		pass


	def __getitem__(self, index):
		return self.__feature_dict.get(index, None)


	def keys(self):
		"""
		Returns:
			(list of strings) Keys of feature_dict
		"""
		return self.__feature_dict.keys()


	def values(self):
		"""
		Returns:
			(list of tf.Tensors) Values of feature_dict
		"""
		return self.__feature_dict.values()


	def items(self):
		"""
		Returns:
			(list of Key:Value pairs) Key:Value pairs of feature_dict
		"""
		return self.__feature_dict.items()


	def epochs(self):
		"""
		Returns:
			(uint) Count of epochs
		"""
		return self.__epochs


	def load(self, feature_json, name_k='name', in_type_k='in_type', cast_type_k='cast_type', fix_len_k='fix_len'):
		"""
		Load & parse record by given feature_json.
		Args:
			feature_json: (string) Path to JSON-file e.g. config/head_feature.json
			name_k:       (string) Retarget key for 'name' in feature_json
			in_type_k:    (string) Retarget key for 'in_type' in feature_json
			cats_type_k:  (string) Retarget key for 'cast_type' in feature_json
			fix_len_k:    (string) Retarget key for 'fix_len' in feature_json
		"""
		feature_json = load_json(feature_json)

		for key, val in feature_json.items():
			f = Feature(
				name = val[name_k],
				in_type = val[in_type_k],
				cast_type = val.get(cast_type_k, None), #optional 
				fix_len = val.get(fix_len_k, None)) #optional
			self.__feature_dict[key] = f
			setattr(self, key, f)
		return self


	def parser_dict(self):
		"""
		Creates and returns parser_dict from feature_dict.
		Returns:
			(dict of tf.Features)
		"""
		if self.__parser_dict:
			return self.__parser_dict
		for f in self.__feature_dict.values():
			if f.fix_len != None:
				self.__parser_dict[f.name] = tf.FixedLenFeature(f.fix_len, dtype=f.in_type)
			else:
				self.__parser_dict[f.name] = tf.VarLenFeature(dtype=f.in_type)
		return self.__parser_dict


	def read(self, record_files, epochs=None):
		"""
		Start to read record iterativly.
		Args:
			record_files: (string) Path to Tensorflow-Record-File
			expors:       (uint) Count of epochs
		"""
		if epochs:
			self.__epochs = max(1, epochs)
		example = read(record_files, self.parser_dict(), self.reader, self.__epochs)
		for key, f in self.__feature_dict.items():
			tensor = example[f.name]
			if f.cast_type:
				tensor = tf.cast(example[f.name], f.cast_type)
			self.__feature_dict[key].tensor = tensor
		return self


def load_examples(feature_list, record_list, epochs=1):
	"""
	Load list of Examples.
	Each Example is an iterator of a record.
	Args:
		feature_list: (list of strings) Paths to feature.json
		              e.g. 'config/head_feature.json'
		record_list:  (list of strings) Paths to Tensorflow-Records.
		epochs:       (uint) Count of epochs.
		              How many times the record will be read.
	Retunrs:
		(list of Example)
	Note:
		List contains Examples for each feature.json on record.
		E.g. (2 feature.json * 3 record) = 6 Example
	"""
	if len(feature_list) == 1:
		from glob import iglob
		features = iglob(feature_list[0])
	else:
		features = feature_list
	return [Example(f, epochs=epochs).read(record_list) for f in features]

