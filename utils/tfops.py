# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig


##INSTALLED##
import tensorflow as tf


def lower_str(tf_str):
	def op(s):
		return s.lower()
	if type(tf_str) is str:
		return op(tf_str)
	else:
		return tf.py_func(op, [tf_str], tf.string, stateful=False)


def str_in(tf_str, any_str):
	def op(s, l):
		return (s in l)
	if type(tf_str) is str:
		return op(tf_str, any_str)
	else:
		return tf.py_func(op, [tf_str, any_str], tf.bool, stateful=False)


def split_str(tf_str, delimiter, select):
	def op(s, dl, sel):
		return s.split(dl)[sel]
	if type(tf_str) is str:
		return op(tf_str, delimiter, select)
	else:
		return tf.py_func(op, [tf_str, delimiter, select], tf.string, stateful=False)


def tf_shape(shape):
	def op(shape):
		return tf.TensorShape(s for s in shape)
	return tf.py_func(op, [shape], tf.int32, stateful=False)


def decode_image(image, shape, encoding='png', boxsize=0, channels=3):
	def raise_error(e):
		raise ValueError('ERROR: encoding of "{}" not supported'.format(e))

	with tf.variable_scope('decode_image'):
		e = lower_str(encoding)

		image = tf.cond(str_in(e, 'png'), #ELIF
			lambda: tf.image.decode_png(image, dtype=tf.uint8), #THEN
			lambda: tf.cond(str_in(e, ['jpg', 'jpeg']), #ELIF
			lambda: tf.image.decode_jpeg(image), #THEN
			lambda: tf.py_func(raise_error, [e], tf.uint8, stateful=False))) #ELSE
		image = tf.reshape(image, shape)

		if isinstance(boxsize, tf.Tensor):
			#TODO
			raise NotImplementedError("Solve boxsize as a Tensor is not implemented yet.")
		elif boxsize == 0:
			#TODO how to fix shape????
			#image.set_shape(tf_shape(shape))
			asp_0 = tf.cast(shape[0], dtype=tf.int32, name='asp_0')
			asp_1 = tf.cast(shape[1], dtype=tf.int32, name='asp_1')
		else:
			asp_0 = tf.cast(shape[0] * boxsize / shape[1], dtype=tf.int32, name='asp_0')
			asp_1 = tf.cast(shape[1] * boxsize / shape[0], dtype=tf.int32, name='asp_1')
			tf_box = tf.constant(boxsize)
			size = tf.cond(asp_0 < asp_1, #IF
				lambda: tf.tuple([asp_0, tf_box]), #ELSE
				lambda: tf.tuple([tf_box, asp_1]),
				name='size')
			image = tf.image.resize_images(image, size)
			image = tf.image.pad_to_bounding_box(image, 0, 0, boxsize, boxsize)
			image.set_shape([boxsize, boxsize, channels])
	return image, asp_0, asp_1


