# KonHandsCPM
The Convolutional Pose Machine of S.-E. Wei et al. XXX translated to TensorFlow by T. Ho et al. XXX and adapted for the KONSENS-NHE project XXX.
## Objectives
A framework for Grasp-Pose-Estimation is requested to control the process of grasping from an egocentric view. The Convolutional Pose Machine enables to detect 2D-Keypoints or rather 2D-Joints of the hand and even more. This modul might become the basic component of the Grasp-Pose-Estimation.

## Tested Environment & Requirements
KonHandsCPM was build and tested on following environment:
 - GeForce GTX 1080 Ti
 - Ubuntu 16.04
 - CUDA9
 - TensorFlow 1.6
 - OpenCV 3.1
 - Numpy
 - Matplotlib
 - Pillow

## Simple Demo Usage
This example shows how to run a simple demo with the pre-trained model of T. Ho et al. for natural hands.

 1. Clone or download this project to a compatible environment like mentioned before.
 2. Make sure that all submodules are downloaded:
``$ git submodule init && git submodule update``
 4. Download a pre-trained model (see: XXX).
 5. Go to the directory of ``demo.py``.
 6. Run ``demo.py`` as follows:
```` 
$ python3 demo.py \
 --images = testhand.jpg \
 --joints = config/joints.json \
 --model = model/cpm_hand.pkl \
 --boxsize = 384 \
 --imgtype = BGR \
 --stages = 6 \
 --layers = 22 \
 --visualize = cv2 mpl
````

## Advanced Usage
 
### Create Dataset
Create your own synthetic dataset with KonHandsPoser (see: XXX)

Or download and make use of compatible datasets e.g.:
 - CMU Panoptic Dataset
 - UCI-EGO (recorder not available yet!)
 - SynthHands (recorder not available yet!)

### Record Dataset
 
### Run Training

### Run Evaluation
 
## License
This project is licensed under the Apache 2.0 License.