#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig

###BUILDIN###
import os
##INSTALLED##
import numpy as np
import cv2
import tensorflow as tf
####LOCAL####
from utils.tfio import load_examples, tf_string, tf_strings, tf_ints, tf_floats
from utils.tfops import decode_image, split_str
from utils.tools import delta, load_labels
import utils.tools.vizcv as viz
from graph.cpm import CPM
from graph import load_weights, check_weights

#TODO create Evaluation class!!!

DELIMITER = '/'


def argparser(parents=None):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=__file__
	)
	parser.add_argument('-r', '--records', help='Tensorflow record files of evaluation set.', type=str, nargs='*', default=['data/egodexter/desk-val.record'], metavar='RECORD')
	parser.add_argument('-m', '--model', help='Model to evaluate.', type=str, default='model/cpm_hand.pkl', metavar='FILE')
	parser.add_argument('-f', '--features', help='JSON of feature description.', type=str, nargs='*', default=['config/egodexter3D-features.json'], metavar='JSON')
	parser.add_argument('-j', '--joints', help='JSON of joint labels.', type=str, default='config/joints.json', metavar='JSON')
	parser.add_argument('-l', '--logdir', help='Output dir where log files might be saved to.', type=str, default=None, metavar='DIR')
	parser.add_argument('-o', '--output', help='Output file to save numpy.array of losses.', type=str, default=None, metavar='FILE')
	parser.add_argument('-s', '--steps', help='How many steps of evaluation to run (-1 = ignore steps).', type=int, default=-1, metavar='INT')
	parser.add_argument('-e', '--evaluate', help='List of selected joints for evaluation.', type=str, nargs='*', default=['thumb_tip', 'index_tip', 'middle_tip', 'ring_tip', 'pinky_tip'], metavar='JOINT')
	parser.add_argument('-t', '--imgtype', help='The type of input images the model has to expect.', type=str, choices=['BGR', 'RGB', 'GRAY'], default='BGR')
	parser.add_argument('-B', '--boxsize', help='The box size of input images the model has to expect.', type=int, default=512, metavar='INT')
	parser.add_argument('-S', '--stages', help='Stages in CPM model', type=int, default=6, metavar='INT')
	parser.add_argument('-V', '--visualize', help='Visualize each step.', type=bool, nargs='?', const=True, default=False, metavar='')
	parser.add_argument('-F', '--flip', help='Flip input.', type=bool, nargs='?', const=True, default=False, metavar='')
	parser.add_argument('-R', '--record', help='Record goodies with a lose below given threshold.', type=str, default=None, metavar='FILE')
	parser.add_argument('-T', '--threshold', help='Threshold to determine goodies for record', type=float, default=0.2, metavar='FLOAT')
	parser.add_argument('-D', '--depth_scale', help='Use depth in heatmaps. (0 = disable)', type=float, default=0, metavar='FLOAT')
	parser.add_argument('-L', '--layers', help='Count of heatmap layers (0 <= take count of joints).', type=int, default=0, metavar='INT')
	return parser


def show(images, uvd_gt, fin_uvd, fin_hmaps, joint_dict=None, selected=None, imgtype='BGR'):
	def destroy():
		for win in show.winames.values():
			cv2.destroyWindow(win)
		pass
	show.destroy = destroy
	show.winames = {}

	for i in range(images.shape[0]):
		if imgtype == 'RGB':
			img = images[i,:,:,::-1].copy()
		else:
			img = images[i].copy()
		hmap = viz.sum_hmaps(fin_hmaps[i], joint_dict)
		hmap = cv2.resize(hmap, img.shape[:2])
		if joint_dict and selected:
			hmap = viz.draw_arrows(hmap, uvd_gt[i], fin_uvd[i], joint_dict, selected)
		hmap = hmap * 128
		win = 'batch {} evaluation'.format(i)
		show.winames[i] = win
		img = np.clip(img + hmap, 0, 255).astype(np.uint8)
		cv2.imshow(win, img)
	return cv2.waitKey(0)


def record_image(image, features, base_key='image'):
	from PIL import Image
	from io import BytesIO
	shape = image.shape
	image = Image.fromarray(image.astype(np.uint8), 'RGB')
	encoded = BytesIO()
	image.save(encoded, 'PNG')
	features[DELIMITER.join([base_key, 'filename'])] = tf_string('.png'.encode('utf8'))
	features[DELIMITER.join([base_key, 'shape'])] = tf_ints(shape)
	features[DELIMITER.join([base_key, 'encoded'])] = tf_string(encoded.getvalue())
	return features, shape


def record_joints(uvd, joint_dict, features, base_key='joint'):
	joint_name = [None] * len(joint_dict)
	uvd = uvd.astype(np.float32)
	for name, joint in joint_dict.items():
		joint_name[joint.idx] = name.encode('utf8')
	features[DELIMITER.join([base_key, 'name'])] = tf_strings(joint_name)
	features[DELIMITER.join([base_key, 'u'])] = tf_floats(uvd[:,0])
	features[DELIMITER.join([base_key, 'v'])] = tf_floats(uvd[:,1])
	features[DELIMITER.join([base_key, 'd'])] = tf_floats(uvd[:,2])
	return features


def record_heatmaps(uvd, features, boxsize=512, depth_scale=0, base_key='joint'):
	from utils.tools.vizcv import create_heatmaps
	uvd_len = len(uvd)
	uvg = [None] * uvd_len
	for idx in range(uvd_len):
		if depth_scale:
			gauss = depth_scale / float(uvd[idx,2])
		else:
			gauss = 1
		uvg[idx] = (
			float(uvd[idx,0]),
			float(uvd[idx,1]),
			gauss
		)
	heatmaps = create_heatmaps(boxsize//8, uvg)
	features[DELIMITER.join([base_key, 'heatmaps'])] = tf_floats(heatmaps.reshape(-1))
	return features


def record_example(recorder, images, uvd, joint_dict, features, depth_scale=0, imgtype='RGB'):
	if imgtype == 'BGR':
		images = images[:,:,:,::-1]
	for i in range(images.shape[0]):
		_, shape = record_image(images[i], features, 'scene/head/image')
		record_joints(uvd[i][:-1], joint_dict, features, 'scene/head/joint')
		record_heatmaps(uvd[i][:-1], features, shape[0], depth_scale, 'scene/head/joint')
		tf_example = tf.train.Example(features=tf.train.Features(feature=features))
		recorder.write(tf_example.SerializeToString())
	return features


def evaluate(sess, cpm, joint_dict, selected, steps=-1, visualize=False, threshold=0.2, depth_scale=0):
	## Start threads? ##
	coord = tf.train.Coordinator()
	threads = tf.train.start_queue_runners(coord=coord)
	summary = tf.summary.merge_all()
	selected_len = len(selected)
	report = []
	step = 0
	
	try: ## Loop up to step if set, otherwise run dataset N epochs ##
		while steps < 0 or step < steps:
			step += 1
			model_state = [
				cpm.input_img, 
				cpm.uvd_gt,
				cpm.final_uvd,
				cpm.final_heatmap,
				cpm.final_eucl_loss,
				summary
			]
			images, uvd_gt, fin_uvd, fin_hmaps, fin_loss, summ = sess.run(model_state)			
			fin_loss = fin_loss / selected_len # Normalize fin_loss
			report.append(fin_loss)
			print("Final loss: {}".format(fin_loss))
			if visualize or cpm.recorder:
				images = (images + 0.5) * 127
			if cpm.logger:
				cpm.logger.add_summary(summ, step)
			if cpm.recorder:
				if fin_loss < threshold:
					record_example(cpm.recorder, images, fin_uvd, joint_dict, {}, depth_scale, cpm.img_type)
					print("Recorded example!")
			if visualize:
				key = show(images, uvd_gt, fin_uvd, fin_hmaps, joint_dict, selected, cpm.img_type)
				if key is ord('h'):
					visualize = False
					show.destroy()
				elif key is 27:
					exit("Quit request!")
			
	except tf.errors.OutOfRangeError:
		print("INFO: Dataset expired at step {}".format(step))
	else:
		print("Evaliation done.")
	finally:
		## Join threads? ##
		coord.request_stop()
		coord.join(threads)
	return report


def prep_example(features, records, boxsize):
	with tf.variable_scope('examples'):
		examples = load_examples(features, records)
		if not examples:
			raise RuntimeError("Examples is empty! Check data record.")
		for example in examples:
			fn = example.image_filename.tensor
			ext = split_str(fn, '.', -1) #'png'
			shape = example.image_shape.tensor
			image = example.image_encoded.tensor
			image, asp_0, asp_1 = decode_image(image, shape, ext, boxsize)
			if args.flip:
				image = tf.image.flip_left_right(image)
			if args.imgtype == 'BGR':
				image = image[:,:,::-1]
			image = tf.cast(image, tf.float32)
			image = tf.add(tf.divide(image, 255), -0.5)
			example.image = image

			shape = tf.cast(shape, tf.float32, name='shape')
			tf_boxsize = tf.constant(boxsize, tf.float32, name='boxsize')
			if args.flip:
				u = tf.subtract(1.0, example.joint_u.tensor)
			else:
				u = example.joint_u.tensor
			asp_0 = tf.cast(asp_0, tf.float32)
			asp_1 = tf.cast(asp_1, tf.float32)
			v = (example.joint_v.tensor - (tf_boxsize - asp_1) / tf_boxsize) * (asp_0 / tf_boxsize) 
			d = example.joint_d.tensor / -100.0 + 1
			example.uvd = tf.stack([u, v, d], axis=1)

		input_img = tf.stack([e.image for e in examples], axis=0, name='input_img')
		uvd_gt = tf.stack([e.uvd for e in examples], axis=0, name='uvd_gt')
		batch_size = len(examples)
	return input_img, uvd_gt, batch_size


def main(args):
	delta()
	## Load lables ##
	print("\nLoad labels from {}...".format(args.joints))
	joint_dict = load_labels(args.joints)	
	print("Labels loaded.\n")
	## Load examples and init preperation graph ##
	print("Prepare examples...")
	input_img, uvd_gt, batch_size = prep_example(args.features, args.records, args.boxsize)
	print("Examples loaded.\n")

	## Build model graph ##
	print("Build graph...")
	evals =	args.evaluate if args.evaluate else [x[0] for x in sorted(joint_dict.items(), key=lambda x: x[1].idx)]
	layers = len(joint_dict) if args.layers <= 0 else args.layers
	cpm = CPM(
		input_img=input_img,
		boxsize=args.boxsize,
		batch_size=batch_size,
		img_type=args.imgtype)
	cpm.build_graph(args.stages, layers)
	cpm.build_eucl_loss(uvd_gt, joint_dict, evals, dim=2)
	print("Graph ready.\n")

	print("Evaluation Targets: {}\n".format(evals))

	## Session ##
	print("Start session...\n")
	init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
	with tf.Session(config=tf.ConfigProto(device_count={'GPU':1}, allow_soft_placement=True)) as sess:
		## Init all vars ##
		sess.run(init_op)
		## Restore pretrained weights ##
		if args.model.endswith('.pkl'):
			cpm.load_weights(args.model, sess)
		else:
			load_weights(args.model, sess, tf.train.Saver())
		check_weights(sess)

		if args.logdir:
			cpm.logger = tf.summary.FileWriter(args.logdir, sess.graph)
		else:
			cpm.logger = None

		if args.record:
			cpm.recorder = tf.python_io.TFRecordWriter(args.record)
		else:
			cpm.recorder = None

		if args.visualize:
			print("\nPress 'H' to hide visualization.")
			print("Press 'ESC' to abort.\n")

		## Process ##
		report = evaluate(sess, cpm, joint_dict, evals, args.steps, args.visualize, args.threshold, args.depth_scale)

		## Save PCP ##
		if args.output:
			print("\nSave PCK to {}".format(args.output))
			np.save(args.output, report)
		## Show PCP ##
		if args.visualize:
			from utils.tools.vizmpl import plot_pck
			print("\nPlot PCK...")
			plot_pck(report).show()
	return 0

if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)

