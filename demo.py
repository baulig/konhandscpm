#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig


###BUILDIN###
import os
from glob import glob
##INSTALLED##
import numpy as np
import cv2
import tensorflow as tf
####LOCAL####
from graph import check_weights
from graph.cpm import CPM
from utils.tools import delta, load_labels


def argparser(parents=None):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=__file__
	)
	parser.add_argument('-i', '--images', help="Input images.", type=str, nargs='+', default=['testhand.jpg'], metavar='WILDCARD')
	parser.add_argument('-d', '--disparities', help="Disparied images related to input images.", type=str, nargs='*', default=[], metavar='WILDCARD')
	parser.add_argument('-m', '--model', help="Model for demo.", type=str, default='model/cpm_hand.pkl', metavar='FILE')
	parser.add_argument('-j', '--joints', help="JSON of joint labels.", type=str, default='config/joints.json', metavar='JSON')
	parser.add_argument('-B', '--boxsize', help="The box size of input images the model has to expect.", type=int, default=512, metavar='INT')
	parser.add_argument('-T', '--imgtype', help="The type of input images the model has to expect.", type=str, choices=['BGR', 'RGB', 'GRAY'], default='BGR', metavar='TYPE')
	parser.add_argument('-S', '--stages', help="Stages in CPM model", type=int, default=6, metavar='INT')
	parser.add_argument('-V', '--visualize', help="Chose visualization mode. ('none', 'cv2', 'mpl')", type=str, nargs='*', choices=['none', 'cv2', 'mpl'], default='cv2', metavar='MODE')	
	parser.add_argument('-F', '--flip', help="Flip input.", type=bool, nargs='?', const=True, default=False, metavar='')
	parser.add_argument('-L', '--layers', help="Count of heatmap layers (0 <= take count of joints).", type=int, default=0, metavar='INT')
	parser.add_argument('--fov', help="Field of view in degree.", type=float, default=49.1, metavar='FLOAT')
	return parser


def print_uvd(impath, uvd):
	print("image: {}".format(impath))
	for idx, (u, v, d) in enumerate(uvd):
		print("joint{}: {}, {}, {}".format(idx, u, v, d))
	pass


def process_input(impath, sess, cpm, boxsize, flip):
	from utils.tools.hmap import find_maxima
	import utils.tools.vizcv as viz
	###Load image###
	org_img, _ = viz.read_image(impath, boxsize)
	if flip:
		org_img = cv2.flip(org_img, 1)
	test_img = org_img / 255.0 - 0.5
	test_img = np.expand_dims(test_img, axis=0)

	###Inference###
	tensors = [cpm.final_heatmap, cpm.stage_heatmap]
	feed_dict = {cpm.input_img: test_img}
	print("pre process time: {}".format(delta()))
	fin_hmap, _ = sess.run(tensors, feed_dict=feed_dict)
	uvd = find_maxima(fin_hmap[0], as_uvd=True)
	print("inference time: {}".format(delta()))
	return fin_hmap, uvd, org_img


def draw_cv2(fin_hmap, uvd, org_img, labels, boxsize=512, flip=False):
	import utils.tools.vizcv as viz
	hmaps = cv2.resize(fin_hmap[0], (boxsize, boxsize))
	canvas = viz.draw_hmaps(org_img, hmaps, labels)
	canvas = viz.draw_joints(canvas, uvd, labels, colscl=255)
	return canvas


def draw_depthmap(fin_hmap, boxsize=512, winame='depthmap'):
	dmap = np.clip(fin_hmap[0,:,:,-2]*255, 0, 255).astype(np.uint8)
	dmap = cv2.resize(dmap, (boxsize, boxsize))
	cv2.imshow(winame, dmap)
	return dmap


#fr = 0
def use_cv2(impaths, dispaths, sess, cpm, labels, boxsize=512, flip=False):
	global fr
	for impath in impaths:
		fin_hmap, uvd, org_img = process_input(impath, sess, cpm, boxsize, flip)
		canvas = draw_cv2(fin_hmap, uvd, org_img, labels, boxsize, flip)
		cv2.imshow('demo', canvas)
		#cv2.imwrite('output/frame_{0:4.0f}.jpg'.format(fr), canvas)
		#fr += 1
		print("post process time: {}".format(delta()))
		if cv2.waitKey(0) == 27:
			exit()
	pass


def use_mpl(impaths, dispaths, sess, cpm, labels, boxsize=512, fov=49.1, flip=False, show_cv2=False):
	from utils.tools.pose import fov_to_focal, rectify
	import utils.tools.vizmpl as viz
	from threading import Thread

	def cv2_process():
		while use_mpl.running:
			if use_mpl.update:
				use_mpl.update = False
				for i in range(2):
					if use_mpl.org_img[i] is not None:
						c = draw_cv2(use_mpl.fin_hmap[i], use_mpl.uvd[i], use_mpl.org_img[i], labels, boxsize, flip)
						cv2.imshow('batch{}'.format(i+1), c)
			if cv2.waitKey(1) == 27:
				use_mpl.running = False
		cv2.destroyAllWindows()

	def onkey(event):
		if event.key == 'escape':
			joint_plot.close()
			return

		impath = next(impath_iter, None)
		if dispaths:
			dispath = next(dispath_iter, None)
		else:
			dispath = None
			
		if dispath and impath:
			use_mpl.fin_hmap[0], use_mpl.uvd[0], use_mpl.org_img[0] = process_input(impath, sess, cpm, boxsize, flip)
			use_mpl.fin_hmap[1], use_mpl.uvd[1], use_mpl.org_img[1] = process_input(dispath, sess, cpm, boxsize, flip)
			use_mpl.update = True
			focal = fov_to_focal(fov, deg=True)
			joints3D, cams = rectify(use_mpl.uvd[0][:,:2], use_mpl.uvd[1][:,:2], focal, prob=0.25, threshold=0.005)
			comp = np.concatenate((joints3D, cams))
			maxax = np.absolute(comp).max().max()
			minax = (-maxax, -maxax, -maxax)
			maxax = (maxax, maxax, maxax)
			joint_plot.update(joints3D, cams, minax, maxax)
			event.canvas.draw()
			if not use_mpl.running and show_cv2:
				use_mpl.running = True
				use_mpl.thread.start()
		elif impath:
			use_mpl.fin_hmap[0], use_mpl.uvd[0], use_mpl.org_img[0] = process_input(impath, sess, cpm, boxsize, flip)
			use_mpl.update = True
			joint_plot.update(use_mpl.uvd[0])
			event.canvas.draw()
			if not use_mpl.running and show_cv2:
				use_mpl.running = True
				use_mpl.thread.start()
		else:
			joint_plot.close()
		print("post process time: {}".format(delta()))
		pass

	use_mpl.fin_hmap = [None] * 2
	use_mpl.uvd = [None] * 2
	use_mpl.org_img = [None] * 2
	use_mpl.running = False
	use_mpl.update = False
	if show_cv2:
		use_mpl.cv2_process = cv2_process
		use_mpl.thread = Thread(target=use_mpl.cv2_process)

	impath_iter = iter(impaths)
	if dispaths:
		dispath_iter = iter(dispaths)
	joint_plot = viz.JointPlot(labels, key_release_event=onkey)
	joint_plot.show()
	if use_mpl.running:
		use_mpl.running = False
		use_mpl.thread.join()
	pass


def use_none(impaths, dispaths, sess, cpm, labels, boxsize=512, flip=False):
	for impath in impaths:
		_, uvd, _ = process_input(impath, sess, cpm, boxsize, flip)
		print_uvd(impath, uvd)
	pass


def main(args):
	delta()
	impaths = []
	for imp in args.images:
		if any(i in '?*+[]' for i in imp):
			impaths += sorted(glob(imp))
		else:
			impaths.append(imp)
	
	dispaths = []
	for dis in args.disparities:
		if any(i in '?*+[]' for i in dis):
			dispaths += sorted(glob(dis))
		else:
			dispaths.append(dis)
	
	if dispaths and len(impaths) != len(dispaths):
		raise ValueError("List of input images and disparity images must have same length.")
	
	labels = load_labels(args.joints) #Load labels
	layers = len(labels) if args.layers <= 0 else args.layers
	cpm = CPM( #Build cpm graph
		boxsize=args.boxsize,
		img_type=args.imgtype)
	cpm.build_graph(args.stages, layers)
	init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer()) #Get global and local tensors

	with tf.Session(config=tf.ConfigProto(device_count={'GPU':1}, allow_soft_placement=True)) as sess:
		sess.run(init_op) #Init global and local tensors
		cpm.load_weights(args.model, sess)

		if 'mpl' in args.visualize:
			use_mpl(impaths, dispaths, sess, cpm, labels, args.boxsize, args.fov, args.flip, 'cv2' in args.visualize)
		elif 'cv2' in args.visualize:
			use_cv2(impaths, dispaths, sess, cpm, labels, args.boxsize, args.flip)
		else:
			use_none(impaths, dispaths, sess, cpm, labels, args.boxsize, args.flip)
	pass


if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)
