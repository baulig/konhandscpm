#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig
# Based on: https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md


###BUILDIN###
import os
from io import BytesIO

##INSTALLED##
import numpy as np
import cv2
from PIL import Image
import tensorflow as tf

####LOCAL####
from utils.tools import load_labels
from utils.tfio import *


def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog
	)
	parser.add_argument('images', help="Wildcard of image files.", type=str, nargs='+', metavar='WILDCARD')
	parser.add_argument('-j', '--joints', help="joints.json is a list of joints", type=str, default='config/joints.json', metavar='JSON')
	parser.add_argument('-o', '--outdir', help="Filepath to record output.", type=str, default='data/', metavar='DIR')
	parser.add_argument('-p', '--prefix', help="Prefix for filename of TFRecords.", type=str, default='', metavar='STR')
	parser.add_argument('-b', '--boxsize', help="The box size of input images the model has to expect.", type=int, default=512, metavar='INT')
	return parser


class Recorder():
	def __init__(self, labels):
		self.labels = labels
		self.boxsize = 512
		self.depth_scale = 0
		self.delim='_'

	def record_joints(self, features, base_key='joint'):
		joint_name = [None] * len(self.labels)
		for name, joint in self.labels.items():
			joint_name[joint.idx] = name.encode('utf8')
		features[self.delim.join([base_key, 'name'])] = tf_strings(joint_name)
		fake = tf_floats([-1.0] * len(self.labels))
		features[self.delim.join([base_key, 'u'])] = fake
		features[self.delim.join([base_key, 'v'])] = fake
		features[self.delim.join([base_key, 'd'])] = fake
		return features

	def record_heatmaps(self, features):
		heatmaps = np.zeros((self.boxsize//8, self.boxsize//8, len(self.labels)), np.float32)
		features['heatmaps'] = tf_floats(heatmaps.reshape(-1))
		return features

	def record_depthmap(self, features):
		dmap = np.zeros((self.boxsize//8, self.boxsize//8, 1), np.float32)
		features['depthmap'] = tf_floats(dmap.reshape(-1))
		return features

	def record_image(self, filepath, features, base_key='image'):
		image = cv2.imread(filepath)
		offset = (image.shape[1] - image.shape[0])//2
		image = image[:, offset:image.shape[0]+offset, :]
		image = cv2.resize(image, (self.boxsize, self.boxsize))
		shape = image.shape
		image = Image.fromarray(image.astype(np.uint8), 'RGB')
		encoded = BytesIO()
		image.save(encoded, 'JPEG')

		features[self.delim.join([base_key, 'filename'])] = tf_string(os.path.basename(filepath).encode('utf8'))
		features[self.delim.join([base_key, 'shape'])] = tf_ints(shape)
		features[self.delim.join([base_key, 'encoded'])] = tf_string(encoded.getvalue())
		return features

	def record_example(self, img_path, features):
		self.record_joints(features)
		self.record_heatmaps(features)
		self.record_depthmap(features)
		self.record_image(os.path.join(img_path), features)
		return features


def write_record(index, recorder, output):
	goods = 0
	errs = 0
	key = 0
	with tf.python_io.TFRecordWriter(output) as writer:
		for entry in index:
			try:
				print("Record: {}".format(entry))
				features = recorder.record_example(entry, {})
				tf_example = tf.train.Example(features=tf.train.Features(feature=features))
				writer.write(tf_example.SerializeToString())
				goods += 1
			except:
				errs += 1
				if key != 'a':
					key = input("WARNING: An error occured. Skip example {}. \n\tProceed anyway? (y) [y|n|a]:".format(entry))
				if key == 'n':
					raise
	return goods, errs


def main(args):
	recorder = Recorder(
		labels=load_labels(args.joints)
	)
	recorder.boxsize = args.boxsize

	if not os.path.isdir(args.outdir):
		exit("Outdir '{}' is not a directory".format(args.index))

	example_list = args.images
	num_examples = len(example_list)

	if num_examples:
		print("{} anit training examples.".format(num_examples))
	else:
		exit("List of image paths is empty!")

	train_output_file = os.path.join(args.outdir, args.prefix + 'anti.record')
	train_goods, train_errs = write_record(example_list, recorder, train_output_file)
	print("{} created with {} examples. {} rejected".format(train_output_file, train_goods, train_errs))


if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)
