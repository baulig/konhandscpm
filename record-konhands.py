#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig
# Based on: https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md


###BUILDIN###
import os
import random

##INSTALLED##
import cv2
import tensorflow as tf

####LOCAL####
from utils.tools import load_labels
import utils.tools.retag as rt
from utils.tools.hmap import create_heatmaps
from utils.tfio import *


def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog
	)
	parser.add_argument('-i', '--index', help="index.txt is a list of annotation.xml.", type=str, default='data/index.txt', metavar='TXT')
	parser.add_argument('-j', '--joints', help="joints.json is a list of joints", type=str, default='config/joints.json', metavar='JSON')
	parser.add_argument('-t', '--retarget', help="retarget.json maps xml to dict.", type=str, default='config/retarget.json', metavar='JSON')
	parser.add_argument('-o', '--outdir', help="Filepath to record output.", type=str, default='data/', metavar='DIR')
	parser.add_argument('-p', '--prefix', help="Prefix for filename of TFRecords.", type=str, default='', metavar='STR')
	parser.add_argument('-r', '--ratio', help="How many percent shall be used for validation?.", type=int, default=0, metavar='INT')
	parser.add_argument('-d', '--depth_scale', help="Use depth in heatmaps. (0 = disable).", type=float, default=0, metavar='FLOAT')
	parser.add_argument('-b', '--boxsize', help="The box size of input images the model has to expect.", type=int, default=512, metavar='INT')
	parser.add_argument('-s', '--seed', help="Seed for data shuffling.", type=int, default=42, metavar='INT')
	return parser


class Recorder():
	def __init__(self, labels, retarget_dict):
		self.labels = labels
		self.retarget_dict = retarget_dict
		self.boxsize = 512
		self.depth_scale = 0
		self.pose_key = 'pose'
		self.joint_key = 'joint'
		self.image_key = 'image'
		self.delim='/'

	def extract_pose_info(self, annotations):
		annotations = rt.flatten(annotations)
		return {key:val for key, val in annotations.items() if self.pose_key in key}

	def extract_scene_joints(self, annotations):
		annotations = rt.flatten(annotations, keep=[self.joint_key])
		return {key:val for key, val in annotations.items() if self.joint_key in key}

	def extract_image_info(self, annotations):
		annotations = rt.flatten(annotations, keep=[self.image_key])
		return {key:val for key, val in annotations.items() if self.image_key in key}

	def record_pose_info(self, pose_info, features):
		for key, val in pose_info.items():
			features[key] = tf_string(val.encode('utf8'))
		return features

	def record_joints(self, scene_joints, features):
		for scene, joints in scene_joints.items():
			jlen = len(joints)
			joint_name = [None] * jlen
			joint_u = [0] * jlen
			joint_v = [0] * jlen
			joint_d = [0] * jlen
			for name, label in self.labels.items():
				idx = label.idx
				joint = joints[name]
				joint_name[idx] = name.encode('utf8')
				joint_u[idx] = float(joint['u'])
				joint_v[idx] = float(joint['v'])
				joint_d[idx] = float(joint['d'])
			features[self.delim.join([scene, 'name'])] = tf_strings(joint_name)
			features[self.delim.join([scene, 'u'])] = tf_floats(joint_u)
			features[self.delim.join([scene, 'v'])] = tf_floats(joint_v)
			features[self.delim.join([scene, 'd'])] = tf_floats(joint_d)
		return features

	def record_heatmaps(self, scene_joints, features):
		for scene, joints in scene_joints.items():
			uvg_list = [None] * len(joints)
			for name, label in self.labels.items():
				idx = label.idx
				joint = joints[name]
				if self.depth_scale:
					gauss = self.depth_scale / float(joint['d'])
				else:
					gauss = 1
				uvg_list[idx] = (
					float(joint['u']),
					float(joint['v']),
					gauss
				)
			heatmaps = create_heatmaps(self.boxsize//8, uvg_list)
			features[self.delim.join([scene, 'heatmaps'])] = tf_floats(heatmaps.reshape(-1))
		return features

	def record_depthmap(self, filepath, features, base_key='depth'):
		dmap = cv2.imread(filepath).astype('float32')[:,:,0]
		dmap = cv2.resize(dmap, (self.boxsize//8, self.boxsize//8))
		dmap = 1 - dmap / 255
		features[self.delim.join([base_key, 'depthmap'])] = tf_floats(dmap.reshape(-1))
		return features

	def record_image(self, filepath, features, base_key='image'):
		shape = cv2.imread(filepath).shape
		features[self.delim.join([base_key, 'filename'])] = tf_string(os.path.basename(filepath).encode('utf8'))
		features[self.delim.join([base_key, 'shape'])] = tf_ints(shape)
		with open(filepath, 'rb') as fid:
			features[self.delim.join([base_key, 'encoded'])] = tf_string(fid.read())
		return features

	def record_example(self, xml_path, features):
		base_dir = os.path.dirname(xml_path)
		annotations = rt.retarget(xml_path, self.retarget_dict)
		img_info = self.extract_image_info(annotations)
		pose_info = self.extract_pose_info(annotations)
		scene_joints = self.extract_scene_joints(annotations)
	
		self.record_pose_info(pose_info, features)
		self.record_joints(scene_joints, features)
		self.record_heatmaps(scene_joints, features)
		for key, info in img_info.items():
			self.record_image(os.path.join(base_dir, info['rgb']['filename']), features, key)
			self.record_depthmap(os.path.join(base_dir, info['dpt']['filename']), features, key)
		return features


def write_record(index, recorder, output):
	goods = 0
	errs = 0
	key = 0
	with tf.python_io.TFRecordWriter(output) as writer:
		for entry in index:
			try:
				print("Record: {}".format(entry[0]))
				features = recorder.record_example(entry[0], {})
				tf_example = tf.train.Example(features=tf.train.Features(feature=features))
				writer.write(tf_example.SerializeToString())
				goods += 1
			except:
				errs += 1
				if key != 'a':
					key = input("WARNING: An error occured. Skip example {}. \n\tProceed anyway? (y) [y|n|a]:".format(entry[0]))
				if key == 'n':
					raise
	return goods, errs


def main(args):
	ratio = max(0, min(100, 100 - int(args.ratio)))
	recorder = Recorder(
		labels=load_labels(args.joints),
		retarget_dict=rt.load(args.retarget)
	)
	recorder.boxsize = args.boxsize
	recorder.depth_scale = args.depth_scale

	if not os.path.isdir(args.outdir):
		exit("Outdir '{}' is not a directory".format(args.index))

	print("Reading from {}.".format(args.index))
	with open(args.index) as fid:
		lines = fid.readlines()
	example_list = [line.split() for line in lines]

	if len(example_list) is 0:
		exit("invalid index file {}".format(args.index))

	random.seed(args.seed)
	random.shuffle(example_list)
	num_examples = len(example_list)
	num_train = int(ratio * num_examples * 0.01)
	train_examples = example_list[:num_train]
	val_examples = example_list[num_train:]
	print("{} training and {} validation examples.".format(len(train_examples), len(val_examples)))

	if train_examples:
		train_output_file = os.path.join(args.outdir, args.prefix + 'train.record')
		train_goods, train_errs = write_record(train_examples, recorder, train_output_file)
		print("{} created with {} examples. {} rejected".format(train_output_file, train_goods, train_errs))
	if val_examples:
		val_output_file = os.path.join(args.outdir, args.prefix + 'val.record')
		val_goods, val_errs = write_record(val_examples, recorder, val_output_file)
		print("{} created with {} examples. {} rejected".format(val_output_file, val_goods, val_errs))
	pass


if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)
