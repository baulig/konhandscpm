#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig
# Based on: https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md


###GLOBAL###
import os
import random
import cv2
import tensorflow as tf

###LOCAL###
import utils.tools.retag as rt
from utils.tfio import *

DELIMITER = '/'

def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog
	)
	parser.add_argument('-e', '--egodexter', help="Directory to EgoDexter Desk, Fruits, Kitchen or Rotunda data.", type=str, nargs=1, metavar='DIR')
	parser.add_argument('-o', '--output', help="Filepath to record output.", type=str, default='data/egodexter.record', metavar='FILE')
	parser.add_argument('-i', '--images', help="Choose image folder (e.g. color, color_on_depth, depth).", type=str, default='color', metavar='DIR')
	parser.add_argument('-a', '--annotations', help="Need both annotations (annotation.txt and annotation.txt_3D.txt).", type=str, nargs=2, default=['annotation.txt', 'annotation.txt_3D.txt'], metavar='FILE')
	parser.add_argument('-l', '--labels', help="String array of joint names.", type=str, nargs='*', default=['thumb_tip', 'index_tip', 'middle_tip', 'ring_tip', 'pinky_tip'], metavar='STR')
	parser.add_argument('-d', '--delimiters', help="String of delimiters for splitting joints of annotation.", type=str, nargs=2, default=[';',','], metavar='DELIM')
	return parser

#TODO use 3d annotations only!!!
#TODO calibrate cam params
#TODO create heatmaps!!!
def record_joints(joints2D, joints3D, shape, labels, features, base_key='joint'):
	joint_name = []
	joint_u = []
	joint_v = []
	joint_d = []
	for idx, name in enumerate(labels):
		joint2D = joints2D[idx]
		joint3D = joints3D[idx]
		joint_name.append(name.encode('utf8'))
		joint_u.append(1.4 * float(joint2D[0]) / shape[1] - 0.11)
		joint_v.append(1.0 - float(joint2D[1]) / shape[0])
		joint_d.append(float(joint3D[2].strip()))
	features[DELIMITER.join([base_key, 'name'])] = tf_strings(joint_name)
	features[DELIMITER.join([base_key, 'u'])] = tf_floats(joint_u)
	features[DELIMITER.join([base_key, 'v'])] = tf_floats(joint_v)
	features[DELIMITER.join([base_key, 'd'])] = tf_floats(joint_d)
	return features

#TODO Set image into boxsize
def record_image(imgpath, features, base_key='image'):
	shape = cv2.imread(imgpath).shape
	features[DELIMITER.join([base_key, 'filename'])] = tf_string(os.path.basename(imgpath).encode('utf8'))
	features[DELIMITER.join([base_key, 'shape'])] = tf_ints(shape)
	with open(imgpath, 'rb') as fid:
		features[DELIMITER.join([base_key, 'encoded'])] = tf_string(fid.read())
	return features, shape


def record_tf_example(imgpath, joints2D, joints3D, labels, features):
	_, shape = record_image(imgpath, features)
	record_joints(joints2D, joints3D, shape, labels, features)
	return tf.train.Example(features=tf.train.Features(feature=features))


def create_tf_record(output, imdata, joint_list2D, joint_list3D, labels):
	goods = 0
	errs = 0
	with tf.python_io.TFRecordWriter(output) as writer:
		for idx, (impath, joints2D, joints3D) in enumerate(zip(imdata, joint_list2D, joint_list3D)):
			try:
				tf_example = record_tf_example(impath, joints2D, joints3D, labels, {})
				writer.write(tf_example.SerializeToString())
				goods += 1
			except:
				print("ERROR: Row {} invalid!".format(idx))
				errs += 1	
	return goods, errs


def extract_joints(csv, delimiters):
	def split_line(line):
		return [joints.split(delimiters[1]) for joints in line.split(delimiters[0])[:-1]]
	
	print("Read annotations from {}...".format(csv))
	with open(csv) as fid:
		lines = fid.readlines()
	return [split_line(line.replace(' ', '')) for line in lines]


def main(args):
	from glob import glob
	imdata = sorted(glob(os.path.join(args.egodexter, args.images, '*')))
	csv2D = os.path.join(args.egodexter, args.annotations[0])
	csv3D = os.path.join(args.egodexter, args.annotations[1])
	joint_list2D = extract_joints(csv2D, args.delimiters)
	joint_list3D = extract_joints(csv3D, args.delimiters)

	goods, errs = create_tf_record(args.output, imdata, joint_list2D, joint_list3D, args.labels)
	print("{} created with {} files. {} rejected".format(args.output, goods, errs))

if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)
