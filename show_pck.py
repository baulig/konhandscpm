#!/usr/bin/env python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Author: Gerald Baulig

###BUILDIN###
import os
##INSTALLED##
import numpy as np
####LOCAL####
from utils.tools.vizmpl import plot_pck


def argparser(parents=None):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=__file__
	)
	parser.add_argument('-r', '--reports', help='List of reports files.', type=str, nargs='+', metavar='FILE')
	parser.add_argument('-l', '--labels', help='List of line labels for each PCK file.', type=str, nargs='*', default=[], metavar='LABEL')
	parser.add_argument('-s', '--styles', help='List of line styles for each PCK file. (See matplotlib)', type=str, nargs='*', default=['b-', 'g--', 'r-.', 'c-', 'y--', 'm-.'], metavar='STYLE')
	parser.add_argument('-t', '--title', help='Title of the graph.', type=str, default='PCK', metavar='TITLE')
	parser.add_argument('-b', '--bins', help='Bins of threshold.', type=int, default=20, metavar='INT')
	return parser


def main(args):
	reports_len = len(args.reports)
	if args.labels:
		assert(reports_len <= len(args.labels)), "List of labels must have same length with list of reports"
	else:
		args.labels = [os.path.basename(r).split('.')[0] for r in args.reports]
	if args.styles:
		assert(reports_len <= len(args.styles)), "List of styles must have same length with list of reports"

	reports = [np.load(r) for r in args.reports]
	reports = np.array(reports)

	if args.bins <= 0:
		xs = None
	else:
		r_max = reports.max().max()
		xs = np.arange(0.0, r_max, r_max/args.bins)
	
	for idx in range(reports_len):
		plot_pck(reports[idx], args.labels[idx], args.styles[idx], xs=xs)
	plot_pck.show(args.title)
	return 0


if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)

